public class Byte_ {

    public static void main(String[] args) {
        // -128 al 127
        System.out.println("El tipo Byte corresponde en Byte a: "+ java.lang.Byte.BYTES);
        System.out.println("El tipo Byte corresponde en Bites a: "+ java.lang.Byte.SIZE);
        System.out.println("El valor máximo de un Byte es : "+ java.lang.Byte.MAX_VALUE);
        System.out.println("El valor mínimo de un Byte es : "+ java.lang.Byte.MIN_VALUE);
    }
}
