public class Long_ {
    public static void main(String[] args) {
        // va desde -9223372036854775807 al 9223372036854775807
        System.out.println("El tipo Long corresponde en Byte a: "+ java.lang.Long.BYTES);
        System.out.println("El tipo Long corresponde en Bites a: "+ java.lang.Long.SIZE);
        System.out.println("El valor máximo de un Long es : "+ java.lang.Long.MAX_VALUE);
        System.out.println("El valor mínimo de un Long es : "+ java.lang.Long.MIN_VALUE);
    }
}
