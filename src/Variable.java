
/* VARIABLE  ->  [tipoDato variable]  */


import javax.swing.*;
import java.util.Scanner;

import static java.lang.Math.PI;
import static java.lang.Math.pow;

public class Variable {
    /*Datos Primitivos*/
    int numero = 5;
    double precio = 5.99;
    float latitud = -36.04556666f;
    String nombre = "name";

    char vocal = 'a';

    Boolean isConnected = false;


    //Formas de escribir una variable
    boolean _volar = true;
    boolean $cantar = false;
    boolean saltar = true;


    public static void main(String[] args) {

        /*
         * 1) Declara dos variables numéricas (con el valor que desees), muestra por consola la suma, resta,
         *  multiplicación, división y módulo (resto de la división).
         */

        int numero1 = 10;
        int numero2 = 5;

        System.out.println("----------------------------------------------------------------------------");
        //suma
        System.out.println("La suma de "+numero1+" + "+numero2+" = "+(numero1+numero2));
        //resta
        System.out.println("La resta de "+numero1+" - "+numero2+" = "+(numero1-numero2));
        //multiplicación
        System.out.println("La multiplicación de "+numero1+" * "+numero2+" = "+(numero1*numero2));
        //división
        System.out.println("La división de "+numero1+" / "+numero2+" = "+(numero1/numero2));
        //modulo
        System.out.println("El módulo de "+numero1+" mod "+numero2+" = "+(numero1%numero2));
        //resta

        System.out.println("----------------------------------------------------------------------------");




        /*
        * 2) Convierte un texto en mayúsculas
        */

        String saludar = "Hola mundo desde java";
        System.out.println("Texto Convertido: "+saludar.toUpperCase());

        System.out.println("----------------------------------------------------------------------------");

        /*
        * 3) Declara un String que contenga tu nombre, después muestra un mensaje
        *  de bienvenida por consola. Por ejemplo: si introduzco «Fernando», me aparezca
        *  «Bienvenido Fernando».
        */
        String name = "Fernando";
        System.out.println("<<Bienvenido "+name+">>");
        System.out.println("----------------------------------------------------------------------------");

        String name2 = JOptionPane.showInputDialog("ingrese un nombre por favor");
        JOptionPane.showMessageDialog(null, " <<Bienvenido " + name2+" >>");
        System.out.println("<<Bienvenido "+name2+">>");

        System.out.println("----------------------------------------------------------------------------");

        System.out.println("----------------<<< 5) Calcular el área de un circulo >>> -------------------------------");

        System.out.println("Ingrese el radio del circulo");
        Scanner leer = new Scanner(System.in);
        double radio = Double.parseDouble(leer.nextLine());
        System.out.println("El area del circulo de radio "+radio+" es:"+(PI*pow(radio,2)));
    }
}
