public class Short_ {

    public static void main(String[] args) {
        // va desde -32767 al 32767
        System.out.println("El tipo Short corresponde en Byte a: "+ java.lang.Short.BYTES);
        System.out.println("El tipo Short corresponde en Bites a: "+ java.lang.Short.SIZE);
        System.out.println("El valor máximo de un Short es : "+ java.lang.Short.MAX_VALUE);
        System.out.println("El valor mínimo de un Short es : "+ java.lang.Short.MIN_VALUE);
    }
}
