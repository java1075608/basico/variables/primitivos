public class Double_ {
    public static void main(String[] args) {
        // va desde 4.9E-324 al 1.7976931348623157E308
        System.out.println("El tipo Double corresponde en Byte a: "+ java.lang.Double.BYTES);
        System.out.println("El tipo Double corresponde en Bites a: "+ java.lang.Double.SIZE);
        System.out.println("El valor máximo de un Double es : "+ java.lang.Double.MAX_VALUE);
        System.out.println("El valor mínimo de un Double es : "+ java.lang.Double.MIN_VALUE);
    }
}
