public class Integer_ {
    public static void main(String[] args) {
        // va desde -2147483647 al 2147483647
        System.out.println("El tipo Integer corresponde en Byte a: "+ java.lang.Integer.BYTES);
        System.out.println("El tipo Integer corresponde en Bites a: "+ java.lang.Integer.SIZE);
        System.out.println("El valor máximo de un Integer es : "+ java.lang.Integer.MAX_VALUE);
        System.out.println("El valor mínimo de un Integer es : "+ java.lang.Integer.MIN_VALUE);
    }
}
