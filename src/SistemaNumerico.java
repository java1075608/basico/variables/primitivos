public class SistemaNumerico {
    public static void main(String[] args) {
        System.out.println("--------------------PASAR DE UN NÚMERO ENTERO A BINARIO--------------------");
        int numero = 500;
        System.out.println("numeroDecimal = " + numero);
        System.out.println("numero binario de: " + numero + " es = " + Integer.toBinaryString(numero));
        System.out.println("---------------------------------------------------------------------------");
        System.out.println("--------------------PASAR DE UN NÚMERO ENTERO A OCTAL--------------------");
        System.out.println("numero octal de: " + numero+" es = "+Integer.toOctalString(numero));
        System.out.println("---------------------------------------------------------------------------");
        System.out.println("--------------------PASAR DE UN NÚMERO ENTERO A HEXADECIMAL--------------------");
        System.out.println("numero hexadecimal de: " + numero+" es = "+Integer.toHexString(numero));
    }
}
